import express from "express";
import json from "body-parser";

export const router = express.Router();

export default { router };

// Configurar de la primer ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Practicas de nodeJS" });
});

router.get("/tabla", (req, res) => {
  const params = {
    numero: req.query.numero,
  };
  res.render("partials/practicas/tabla", params);
});

router.post("/tabla", (req, res) => {
  const params = {
    numero: req.body.numero,
  };
  res.render("partials/practicas/tabla", params);
});


//Practica cotizacion
router.get("/cotizacion", (req, res) => {
  const params = {
    valor: req.query.valor,
    pInicial: req.query.pInicial,
    plazos: req.query.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});

router.post("/cotizacion", (req, res) => {
  const params = {
    valor: req.body.valor,
    pInicial: req.body.pInicial,
    plazos: req.body.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});
