import express from 'express';

import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';

import misRutas from './router/index.js';

const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
// Crear la aplicación Express
const app = express();

// Asignaciones
app.set("view engine", "ejs");
app.use(json.urlencoded({extends:true}));

// Asignar al objeto información
app.set(express.static(__dirname + '/public'));
app.use(misRutas.router);

app.listen(puerto, () =>{
    console.log("Iniciando el servidor por el puerto " + puerto);
});



//para correrlo en la terminal se usa el comando: nodemon app.js
//aws
//localhost:3000/tabla



